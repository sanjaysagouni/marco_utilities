package at.mavila.rx.db.util;

import at.mavila.rx.db.pojos.DolMiVariables;
import com.github.davidmoten.rx.jdbc.Database;
import com.github.davidmoten.rx.jdbc.tuple.TupleN;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 14.09.2015.
 */
public class ConnectionFactoryGenericTest {


        private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionFactoryGenericTest.class);

        @Before
        public void init(){
                BasicConfigurator.configure();
        }


        @Test public void testWithOracle() throws Exception {

                final String url = "jdbc:oracle:thin:@PCStest5:1521:DOLd59";
                final String username = "dolphin";
                final String password = "dolphintest";
                final String driver = "oracle.jdbc.OracleDriver";

                ConnectionFactory connectionFactory = new ConnectionFactoryGeneric(url, username, password, driver);
                Connection connection = connectionFactory.getConnection();
                //Connected?
                assertThat(connection.isClosed()).isFalse();

                String query = "SELECT VAR, TO_CHAR(VAR_VALUE) as VAR_VALUE,TO_CHAR(VALID_FROM) as VALID_FROM,to_Char(VALID_UNTIL) as VALID_UNTIL,TARIF from DOL_MI_VARIABLES where VALID_FROM < SYSDATE and VAR is not null and ROWNUM <= 100 order by VAR asc";
                //Database database = new Database(connection);


                List<DolMiVariables> dolMiVariables = new ArrayList();

                /*We are familiar with this, don't we?*/
                Statement stat = connection.createStatement();
                ResultSet rs = stat.executeQuery(query);
                while (rs.next()) {
                        //Person person = new Person(rs.getInt("id"), rs.getString("firstName"));
                        final DolMiVariables dolMiVariable = new DolMiVariables(
                                rs.getString("VAR"),
                                rs.getString("VAR_VALUE"),
                                rs.getString("VALID_FROM"),
                                rs.getString("VALID_UNTIL"),
                                rs.getString("TARIF"));
                        LOGGER.info(dolMiVariables.toString());
                        dolMiVariables.add(dolMiVariable);
                }
                rs.close();
                connection.close();

                /*In a single line of code we can achieve the same as above.*/
                Observable<DolMiVariables> dolMiVariablesObservable = Database.from(connectionFactory.getConnection()).select(query).autoMap(DolMiVariables.class);
                /*Do whatever you you want with the Observable*/

                /*Recommended not to call toBlocking*/
                final Iterable<DolMiVariables> variables = dolMiVariablesObservable.toBlocking().toIterable();

                LOGGER.info("Iterable class: {}",variables.getClass().getName());

                for(final DolMiVariables dolMiVariablesObject: variables){
                        LOGGER.info("Object:{}", dolMiVariablesObject.toString());
                }

                TupleN<String> tupleN = Database.from(connectionFactory.getConnection()).select(query).getTupleN(String.class).first().toBlocking().single();

                final String value0 = tupleN.values().get(0);
                final String value1 = tupleN.values().get(1);
                final String value2 = tupleN.values().get(2);


                /*Recommended way to use an Observable*/

                /*We could apply operation on the Observable*/
                /*We could filter results*/
                /*Without lambdas: Java < 8*/
                dolMiVariablesObservable
                  .map(new Func1<DolMiVariables, String>() {
                        @Override
                        public String call(DolMiVariables dolMiVariables) {
                                return dolMiVariables.toString();
                        }
                }).map(new Func1<String, String>() {
                        @Override
                        public String call(String s) {
                                return "_______"+s+"________";
                        }
                }).subscribe(new Action1<String>() {
                        @Override
                        public void call(String s) {

                                LOGGER.info("Without lambdas" + s);


                                if (s == null || s.contains("KA_MARKE_G_25")) {
                                        LOGGER.warn("Sending exception.");
                                        throw new NullPointerException("ay que feo");
                                }

                        }
                }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                                LOGGER.error("Ups: "+throwable.toString());
                        }
                });

                /*With Lambdas, Java 8*/
                List<String> listToString = new LinkedList<>();
                dolMiVariablesObservable.
                        map(dolMiVariables1 -> dolMiVariables1.toString()).
                        map(s -> "_______" + s + "________").
                        subscribe(dd -> listToString.add(dd), throwable -> LOGGER.error("Ups: "+throwable.toString()));

                for(final String result : listToString) {
                        LOGGER.info(result);
                }


                assertThat(connection.isClosed()).isTrue();

                assertThat(value0).isNotEmpty();
                assertThat(value1).isNotEmpty();
                assertThat(value2).isNotEmpty();

        }
}