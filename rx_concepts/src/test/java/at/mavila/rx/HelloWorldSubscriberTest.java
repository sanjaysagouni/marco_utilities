package at.mavila.rx;

import junit.framework.TestCase;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by marcoavilac on 03/09/15.
 */
public class HelloWorldSubscriberTest extends TestCase{

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldSubscriberTest.class);

    @Before
    public void setUp() throws Exception {
        BasicConfigurator.configure();
    }

    @Test
    public void testSubscriber() {

        final Action1<String> subscriber = new HelloWorldSubscriber();
        final HelloWorldObservable observable = new HelloWorldObservable();
        final Observable<String> stringObservable = observable.getObservable();
        final Subscription subscription = stringObservable.subscribe(subscriber);
        final boolean subscriptionUnsubscribed = subscription.isUnsubscribed();

        assertThat(subscriptionUnsubscribed).isTrue();

    }


    @Test
    public void testSubscriberChained() {

        //No Lambdas
        Observable.just("hello, world!").subscribe(new Action1<String>() {
            public void call(String s) {
                LOGGER.info(s);
            }
        });

        //Using lambdas.
        Observable.just("hello world").subscribe(s -> LOGGER.info(s));
    }


    @Test
    public void testSubscribedOperators(){

        Observable.just("Hello, world!")
                .map(s -> s + " -Dan")
                .map(s -> s.hashCode())
                .map(i -> Integer.toString(i))
                .subscribe(s -> LOGGER.info(s));

    }

    @Test
    public void testSubscribedOperatorsQuery(){

        Observable.from(new String[]{"ok","ii","kk"}).subscribe(s -> LOGGER.info(s));

    }

}