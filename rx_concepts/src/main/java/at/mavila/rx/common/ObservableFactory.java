package at.mavila.rx.common;

import rx.Observable;

/**
 * Created by 200000313 on 16.09.2015.
 */
public interface ObservableFactory<T> {
        /**
         * Gets an observable
         *
         * @return gets an observable
         */
        Observable<T> getObservable();
}
