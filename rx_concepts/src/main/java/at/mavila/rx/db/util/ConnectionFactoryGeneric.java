package at.mavila.rx.db.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Created by 200000313 on 14.09.2015.
 */
public class ConnectionFactoryGeneric implements ConnectionFactory{

        private final String url;
        private final String username;
        private final String password;
        private final String driver;


        public ConnectionFactoryGeneric(final String url, final String username, final String password,
                final String driver){
                this.url = url;
                this.username = username;
                this.password = password;
                this.driver = driver;


        }

        @Override
        public Connection getConnection() throws ClassNotFoundException, SQLException {
                Class.forName(driver);
                Connection conn = DriverManager.getConnection(url,username,password);
                return conn;
        }
}
