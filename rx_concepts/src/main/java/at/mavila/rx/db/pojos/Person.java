package at.mavila.rx.db.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by marcoavilac on 9/13/2015.
 */

@Entity
public class Person {

        @Id
        @GeneratedValue
        private final Integer id;
        private final String firstName;

        public Person(final Integer id, final String firstName) {
                this.id = id;
                this.firstName = firstName;
        }

        public String getFirstName() {
                return this.firstName;
        }

        public Integer getId() {
                return this.id;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (!(o instanceof Person)) {
                        return false;
                }

                Person that = (Person) o;

                if (!this.getId().equals(that.getId())) {
                        return false;
                }
                return this.getFirstName().equals(that.getFirstName());

        }

        @Override
        public int hashCode() {
                int result = getId().hashCode();
                result = 31 * result + getFirstName().hashCode();
                return result;
        }

        @Override
        public String toString() {
                final StringBuilder sb = new StringBuilder("Person{");
                sb.append("id=").append(id);
                sb.append(", firstName='").append(firstName).append('\'');
                sb.append('}');
                return sb.toString();
        }
}
