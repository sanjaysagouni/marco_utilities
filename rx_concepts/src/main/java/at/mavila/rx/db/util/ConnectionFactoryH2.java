package at.mavila.rx.db.util;

import org.h2.tools.DeleteDbFiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by 200000313 on 14.09.2015.
 */
public class ConnectionFactoryH2 implements ConnectionFactory {


        @Override
        public Connection getConnection() throws ClassNotFoundException, SQLException {
                DeleteDbFiles.execute("~", "test", true);
                Class.forName("org.h2.Driver");
                Connection conn = DriverManager.getConnection("jdbc:h2:~/test");
                return conn;
        }


}
