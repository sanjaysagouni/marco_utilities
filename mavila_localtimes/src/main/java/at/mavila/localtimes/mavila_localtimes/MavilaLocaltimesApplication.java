package at.mavila.localtimes.mavila_localtimes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class MavilaLocaltimesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavilaLocaltimesApplication.class, args);
	}
}
