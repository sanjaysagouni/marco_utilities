package marco_web_app

import at.mavila.Address
import grails.transaction.Transactional

@Transactional
class AddressService {

    Address createAddress(String address){
        final Address address1 = new Address()
        address1.setAddress(address)
        address1.save()
        log.info(String.format("Address created: %s",address1.toString()))
        return address1

    }

    private String generateReferenceNumber() {

        // some specific logic for generating reference number

        return referenceNumber

    }
}
