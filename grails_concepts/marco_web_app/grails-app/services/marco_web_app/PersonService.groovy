package marco_web_app

import at.mavila.Address
import at.mavila.Person
import grails.transaction.Transactional

@Transactional
class PersonService {


    Person createPersonWithAddress(final Person person){
        if(person==null){
            return null
        }
        return person.save()
    }

    def serviceMethod() {

        // some specific logic for generating reference number

        return referenceNumber

    }
}
