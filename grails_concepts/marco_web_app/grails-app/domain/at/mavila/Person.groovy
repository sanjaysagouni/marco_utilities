package at.mavila

class Person {

    String firstName
    List<Address> addressList

    static hasMany = [addressList : Address]


    static constraints = {
        firstName(nullable:false)
    }
}
