package at.mavila

import org.apache.commons.lang3.builder.ToStringBuilder;

class Address {

    String address

    static belongsTo = Person

    static constraints = {
        address(nullable:false)
    }

    @Override
    public String toString(){

        return new ToStringBuilder(this).append("address",address).toString()
    }

}
