import at.mavila.Address
import at.mavila.Person

class BootStrap {

    def addressService
    def personService

    def init = { servletContext ->
        if(Address.count()==0){

            addressService.createAddress("Leskygasse 2")

            addressService.createAddress("Cortigasse 26")
        }

        def listOfAddresses = Address.findAll()

        if(Person.count()==0){

            Person person1 = new Person()
            person1.firstName = "Maclovio"

            Person person2 = new Person()
            person2.firstName = "Pelangocha"

            person1.addressList = new ArrayList<Address>()
            person2.addressList = listOfAddresses

            personService.createPersonWithAddress(person1)
            personService.createPersonWithAddress(person2)

        }


    }
    def destroy = {
    }
}
