package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.Address;
import at.mavila.jpa.pojos.QUser;
import at.mavila.jpa.pojos.User;
import com.mysema.query.types.expr.BooleanExpression;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StopWatch;

import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 10.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring-config.xml" })
@EnableJpaRepositories
@EnableTransactionManagement
public class UserRepositoryTest {

        private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryTest.class);

        @Autowired
        UserRepository userRepository;

        @Autowired
        AddressRepository addressRepository;

        @BeforeClass
        public static void setUp() throws Exception {
                BasicConfigurator.configure();
        }

        /*@Test
        public void testInsertUserAddress() {

                final User user = new User();
                user.setName("agapito");

                final Address address = new Address();
                address.setCity("Mongo City");
                address.setCountry("My country");
                address.setPostcode("1220");
                address.setState("Wien");
                address.setStreet("Anygasse 2");

                user.setAddress(address);

                final Address savedAddress = addressRepository.save(address);
                final User savedUser = userRepository.save(user);

                assertThat(savedAddress).isNotNull();
                assertThat(savedUser).isNotNull();


                //Insert many times
                for(int n=0;n<10000;n++){

                        final User userLoop = new User();
                        userLoop.setName(RandomStringUtils.randomAlphanumeric(10));


                        final Address addressLoop = new Address();
                        addressLoop.setCity(RandomStringUtils.randomAlphanumeric(10));
                        addressLoop.setCountry(RandomStringUtils.randomAlphanumeric(10));
                        addressLoop.setPostcode(RandomStringUtils.randomAlphanumeric(10));
                        addressLoop.setState(RandomStringUtils.randomAlphanumeric(10));
                        addressLoop.setStreet(RandomStringUtils.randomAlphanumeric(10));

                        userLoop.setAddress(addressLoop);


                        final Address savedAddressLoop = addressRepository.save(addressLoop);
                        final User savedUserLoop = userRepository.save(userLoop);

                        assertThat(savedAddressLoop).isNotNull();
                        assertThat(savedUserLoop).isNotNull();


                }
        }*/

        @Test
        @Transactional
        public void testRetrieve() {

                //final List<User> users = this.userRepository.findByName("agapito");

                for(int n=0;n<1000;n++){

                        StopWatch watch0 = new StopWatch("BooleanExpression");
                        watch0.start();
                        final BooleanExpression booleanExpression = QUser.user.name.containsIgnoreCase("ag").or(QUser.user.name.containsIgnoreCase("pito"));
                        watch0.stop();
                        LOGGER.info("BooleanExpression: " + watch0.getTotalTimeMillis());

                        StopWatch watch = new StopWatch("findAll");
                        watch.start();
                        final Iterable<User> users = this.userRepository.findAll(booleanExpression, createPageRequestDesc());
                        watch.stop();
                        LOGGER.info("findAll: " + watch.getTotalTimeMillis());

                        assertThat(users).isNotEmpty();
                        final Iterator<User> userIterator = users.iterator();
                        while (userIterator.hasNext()) {
                                final Address addressInLoop = userIterator.next().getAddress();
                                final Set<User> addressInLoopUsers = addressInLoop.getUsers();
                                final User next = addressInLoopUsers.iterator().next();
                                assertThat(next.getAddress().getStreet()).isNotEmpty();

                        }
                }

        }


        private Pageable createPageRequestDesc() {
                return new PageRequest(0,
                        3,
                        new Sort(Sort.Direction.DESC, "name"));

        }
}