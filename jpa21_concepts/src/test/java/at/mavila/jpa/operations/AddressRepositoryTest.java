package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.Address;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 200000313 on 10.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring-config.xml" })
public class AddressRepositoryTest {


        private static final Logger LOGGER = LoggerFactory.getLogger(AddressRepositoryTest.class);

        @Autowired
        AddressRepository addressRepositiory;

        @BeforeClass
        public static void init(){
                BasicConfigurator.configure();
        }

        @Test
        public void testInsert(){

                final int index = 0;
                final Address address = new Address(new Long(0), "Poribagh " + index, "Dhaka " + index,
                        "X State " + index, "Bangladesh " + index, "1000 " + index, null);

                final Address saved = addressRepositiory.save(address);
                LOGGER.info("Saved: {}",saved);
                assertThat(saved.getAddressId()).isEqualTo(Long.valueOf(1));



        }
}