package at.mavila.jpa.querybuilder;

/**
 * Created by 200000313 on 04.01.2016.
 */
public final class QueryBuilderConstants {

    private QueryBuilderConstants(){
        //No instances of this class to be created.
    }

    //Schemas
    public static final String DOLPHIN = "DOLPHIN";

    //Tables
    public static final String MER_V_GF_SEARCH = "MER_V_GF_SEARCH";
    public static final String MER_V_GF_SEARCH_WITH_NAME = "MER_V_GF_SEARCH_WITH_NAME";

    //Columns.
    public static final String ID = "ID";
    public static final String GF_NUMMER = "GF_NUMMER";
    public static final String ZUNAME = "ZUNAME";

    public static final String OBJEKT = "OBJEKT";
    public static final String FINANZ_ART = "FINANZ_ART";
    public static final String GS_KDNR = "GS_KDNR";
    public static final String LVORNAME = "LVORNAME";
    public static final String LZUNAME = "LZUNAME";
    public static final String VORNAME = "VORNAME";
    public static final String KUNDE = "KUNDE";
    public static final String KDNR = "KDNR";
    public static final String ALT_VTNR = "ALT_VTNR";
    public static final String ENTSCHEIDUNGS_KZ = "ENTSCHEIDUNGS_KZ";
    public static final String SUCH_BEGRIFF = "SUCH_BEGRIFF";
    public static final String KREDITBETRAG = "KREDITBETRAG";
    public static final String LAUFZEIT = "LAUFZEIT";
    public static final String ACC_TIME = "ACC_TIME";
    public static final String PERS_NR = "PERS_NR";
    public static final String GF_CREATED_BRANCH = "GF_CREATED_BRANCH";
    public static final String FINANZIERUNGS_KZ = "FINANZIERUNGS_KZ";
    public static final String ABDECK_ENTHALTEN = "ABDECK_ENTHALTEN";
    public static final String ABDECK_BETRAG = "ABDECK_BETRAG";
    public static final String KREDITART = "KREDITART";
    public static final String PRODCODEPOL_60 = "PRODCODEPOL60";
    public static final String GF_STATUS = "GF_STATUS";
    public static final String RATE = "RATE";
    public static final String ROLLE = "ROLLE";
    public static final String SACHBEARBEITER = "SACHBEARBEITER";
    public static final String WEBORDER_ID = "WEBORDER_ID";
}
