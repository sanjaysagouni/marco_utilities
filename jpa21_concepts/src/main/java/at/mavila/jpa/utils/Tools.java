package at.mavila.jpa.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Marco T. Avila on 18.12.2015.
 */
public final class Tools {


    private static final Pattern CONTAINS_CONTIGUOUS_DOTS = Pattern.compile("(\\.)\\1{1,}");
    private static final Pattern CONTAINS_CONTIGUOUS_COMMAS = Pattern.compile("(\\,)\\1{1,}");
    private static final Pattern CONTAINS_CHARACTERS = Pattern.compile("[^\\d,.-]");
    private static final Pattern IS_LARGE_NUMBER_GERMAN_LOCALE = Pattern.compile("^-?\\d{1,3}(?:\\.\\d{3})*(?:,\\d+)?$");
    private static final Pattern IS_LARGE_NUMBER_US_LOCALE = Pattern.compile("^-?\\d{1,3}(?:,\\d{3})*(?:\\.\\d+)?$");

    private Tools() {
        //Does nothing.
    }

    public static boolean isEmpty(final String string) {
        return string == null || string.trim().isEmpty();
    }


    /**
     * Parse the string and returns a double
     * <p/>
     * getSafeDouble("12334,78") returns 12334.78
     * getSafeDouble("12334.78") returns 12334.78
     * getSafeDouble("12.334,78") returns 12334.78
     * getSafeDouble("12,334.78") returns 12334.78
     * getSafeDouble("12.334.78") returns 0
     * getSafeDouble("1fdssd2vcxvxc33gfdgfd4,78") returns 0
     * getSafeDouble("23,5453453,12,334,78") returns 0
     * getSafeDouble("12334") returns 12334
     * getSafeDouble("abcdekjj") returns 0
     * getSafeDouble("1.07") returns 1.07
     * getSafeDouble("1,07") returns 1.07
     * getSafeDouble("1,0fsdf7") returns 0
     * getSafeDouble(".1") returns 0.1
     * getSafeDouble("-99") returns -99
     * getSafeDouble("-99.75745") returns -99.75745
     * getSafeDouble("-23,5453453,12,334,78") returns 0
     * getSafeDouble("-.75745") returns -0.75745
     * getSafeDouble(" -0.5 ") returns -0.5
     * getSafeDouble(" 0.5 ") returns 0.5
     * getSafeDouble("83883,,83883") returns 0
     * getSafeDouble("834233..834324883") returns 0
     * getSafeDouble("834.2338343.24883..") returns 0
     * getSafeDouble("-0.75745") returns -0.75745
     * getSafeDouble("14") returns 14
     * getSafeDouble("--14") returns 0
     * getSafeDouble("       -14                     ") returns -14
     * getSafeDouble("12,334,543,553,332.44899") returns 12334543553332.44899
     *
     * @param s the string to be parsed
     * @return a parsed double represented by the string or 0 in case of error or an un-parseable string.
     */
    public static double getSafeDouble(final String s) {

        if (isEmpty(s) ||
                CONTAINS_CONTIGUOUS_DOTS.matcher(s.trim()).find() ||
                CONTAINS_CONTIGUOUS_COMMAS.matcher(s.trim()).find() ||
                CONTAINS_CHARACTERS.matcher(s.trim()).find()
                ) {
            return 0;
        }

        try {

            final String toWork = s.trim().startsWith(",") || s.trim().startsWith(".") ? "0" + s.trim() : s.trim();
            final boolean isAGermanLocaleNumber = IS_LARGE_NUMBER_GERMAN_LOCALE.matcher(toWork).matches();
            final boolean isAUSLocaleNumber = IS_LARGE_NUMBER_US_LOCALE.matcher(toWork).matches();

            if (isAGermanLocaleNumber || isAUSLocaleNumber) {
                final Locale locale = isAGermanLocaleNumber ? Locale.GERMAN : Locale.US;
                final DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
                symbols.setDecimalSeparator(isAGermanLocaleNumber ? ',' : '.');
                symbols.setGroupingSeparator(isAGermanLocaleNumber ? '.' : ',');
                return new DecimalFormat("#,##0.###", symbols).parse(toWork).doubleValue();
            }
            return Double.parseDouble(toWork.replace(',','.'));

        } catch (Exception ex) {
            // if there is an error, return 0.
            return 0;
        }

    }

}
