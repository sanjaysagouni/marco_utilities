package at.mavila.jpa.operations;

import at.mavila.jpa.pojos.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by 200000313 on 10.12.2015.
 */
public interface UserRepository extends CrudRepository<User, Long>, QueryDslPredicateExecutor<User> {

        @Query(" from User u where u.name=:name")
        List<User> findByName(@Param("name") final String name);


}
