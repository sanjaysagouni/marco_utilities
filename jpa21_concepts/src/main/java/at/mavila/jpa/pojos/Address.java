package at.mavila.jpa.pojos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Address.
 */
@Entity
@Table(name = "SAMPLE_ADDRESS")
public class Address implements Serializable {
    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -6152200241673922361L;
    @Id
    @SequenceGenerator(name = "address_id_seq", sequenceName = "address_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_seq")
    private Long addressId;

    @Column(name = "street", nullable = false, length = 255)
    private String street;
    @Column(name = "city", nullable = false, length = 255)
    private String city;
    @Column(name = "state", nullable = false, length = 255)
    private String state;
    @Column(name = "country", nullable = false, length = 20)
    private String country;
    @Column(name = "postcode", nullable = false, length = 50)
    private String postcode;

    /*
    * Joins
    * */
    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    private Set<User> users = new HashSet<User>();

    public Address(){
        //Does nothing.
    }



    public Address(final Long id, final String street, final String city, final String state, final String country, final String postcode, Set<User> users) {
        this.addressId = id;
        this.street = street;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postcode = postcode;
        if(users!=null) {
            this.users = users;
        }
    }

    public Long getAddressId() {
        return this.addressId;
    }


    public String getStreet() {
        return this.street;
    }


    public String getCity() {
        return this.city;
    }


    public String getState() {
        return this.state;
    }


    public String getCountry() {
        return this.country;
    }


    public String getPostcode() {
        return this.postcode;
    }

    public Set<User> getUsers(){return this.users;}


    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void addUser(final User user) {
        this.users.add(user);
        if(user.getAddress()!= this)
        {
            user.setAddress(this);
        }
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        final ReflectionToStringBuilder reflectionToStringBuilder = new ReflectionToStringBuilder(this,ToStringStyle.SHORT_PREFIX_STYLE );
        reflectionToStringBuilder.setExcludeFieldNames("users");
        return reflectionToStringBuilder.toString();
    }

}
