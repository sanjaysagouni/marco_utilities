package at.mavila.jpa.pojos;

import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.PathMetadataFactory;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathInits;
import com.mysema.query.types.path.StringPath;

/**
 * Created by 200000313 on 11.12.2015.
 */
public class QUser extends EntityPathBase<User> {

        private static final long serialVersionUID = 3093776169920527494L;

        private static final PathInits INITS = PathInits.DIRECT2;
        public static final QUser user = new QUser("user");
        public final NumberPath<Long> userId = createNumber("userId", Long.class);
        public final StringPath name = createString("name");

        public NumberPath<Long> getUserId() {
                return userId;
        }

        public StringPath getName() {
                return name;
        }

        public static QUser getUser() {
                return user;
        }

        public QUser(String variable) {
                this(User.class, PathMetadataFactory.forVariable(variable), INITS);
        }

        private QUser(Class<? extends User> type, PathMetadata<?> metadata, PathInits inits) {
                super(type, metadata, inits);
        }

}
