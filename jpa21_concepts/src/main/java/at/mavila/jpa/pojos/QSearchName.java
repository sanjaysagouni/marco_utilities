package at.mavila.jpa.pojos;

import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.PathMetadataFactory;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathInits;
import com.mysema.query.types.path.StringPath;

/**
 * Created by 200000313 on 22.12.2015.
 */
public class QSearchName extends EntityPathBase<SearchName> {


        private static final PathInits INITS = PathInits.DIRECT2;
        public static final QSearchName user = new QSearchName("searchName");
        public final StringPath zuname = createString("zuname");



        public final StringPath vorname = createString("vorname");



        public final NumberPath<Long> gfCreatedBranch = createNumber("gfCreatedBranch", Long.class);

        public QSearchName(Class<? extends SearchName> type, PathMetadata<?> metadata) {
                super(type, metadata);
        }

        public QSearchName(String variable) {
                this(SearchName.class, PathMetadataFactory.forVariable(variable), INITS);
        }

        private QSearchName(Class<? extends SearchName> type, PathMetadata<?> metadata, PathInits inits) {
                super(type, metadata, inits);
        }


        public StringPath getVorname() {
                return vorname;
        }

        public StringPath getZuname() {
                return zuname;
        }

        public static QSearchName getUser() {
                return user;
        }

        public NumberPath<Long> getGfCreatedBranch() {
                return gfCreatedBranch;
        }
}
