package at.mavila.jpa.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by 200000313 on 22.12.2015.
 */
@Entity
@Table(name = "MER_V_GF_SEARCH_WITH_NAME")
public class SearchName extends CoreSearch {

        /*
        * serialVersionUID.
        * */
        private static final long serialVersionUID = 7405764970925663067L;

        @Column(name = "OBJEKT")
        private String objekt;

        public String getObjekt() {
                return objekt;
        }

        public void setObjekt(String objekt) {
                this.objekt = objekt;
        }

        @Override
        public String toString() {
                final StringBuilder sb = new StringBuilder("SearchName{");
                sb.append("objekt='").append(objekt).append('\'');
                sb.append(", ").append(super.toString());
                sb.append('}');
                return sb.toString();
        }
}
