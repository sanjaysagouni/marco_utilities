package at.mavila.exercises.duplicates.pojos;

import at.mavila.exercises.duplicates.exceptions.InvalidIdException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.Validate.notNull;

public class Person implements Comparable<Person> {

    private final Long id;
    private final String firstName;
    private final String secondName;


    private Person(final Builder builder) {
        final Long id = builder.id;
        Validate.notNull(id, "id must not be null, please fill it.");
        this.id = id;
        this.firstName = builder.firstName;
        this.secondName = builder.secondName;
    }


    public Long getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;

        if (object == null || getClass() != object.getClass()) return false;

        Person that = (Person) object;

        return new EqualsBuilder()
                .append(this.getId(), that.getId())
                .append(this.getFirstName(), that.getFirstName())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(1, 1)
                .append(this.id)
                .append(this.firstName)
                .toHashCode();
    }

    @Override
    public int compareTo(Person person) {
        notNull(person, "Person mus not be null");

        final int thisHashCode = this.hashCode();
        final int thatHashCode = person.hashCode();
        final int i = thisHashCode ^ thatHashCode;

        if (i == 0) {
            return 0;
        }

        if (thisHashCode > thatHashCode) {
            return 1;
        }

        return -1;

    }

    public static class Builder {

        private Long id;
        private String firstName;
        private String secondName;

        public Builder id(final Long personIdP) {

            final Long personId = ofNullable(personIdP).orElseThrow(() -> new InvalidIdException("Expected an id"));
            final int personIdComparison = personId.compareTo(0L);
            if (personIdComparison <= 0) {
                throw new InvalidIdException("Id must not be negative or zero");
            }

            this.id = personId;
            return this;
        }

        public Builder firstName(final String first) {
            this.firstName = first;
            return this;
        }

        public Builder secondName(final String second) {
            this.secondName = second;
            return this;
        }

        public Person build() {
            return new Person(this);
        }


    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("firstName", firstName)
                .append("secondName", secondName)
                .toString();
    }
}
