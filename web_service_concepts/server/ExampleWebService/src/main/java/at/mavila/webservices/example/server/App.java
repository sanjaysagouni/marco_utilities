package at.mavila.webservices.example.server;

import java.util.Calendar;
import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initial web app
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class App {

	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	public App() {
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug("App loaded");

		}
	}

	@WebMethod(operationName = "sayHello")
	public String sayHello(@WebParam(name = "guestname") String guestname) {

		if (guestname == null || guestname.trim().isEmpty()) {
			return "Hello (unknown name)";
		}
		return "Hello " + guestname;

	}

	@WebMethod(operationName = "calculateMonths")
	public int calculateMonths(@WebParam(name = "start") final Date dateStart,
			@WebParam(name = "end") final Date dateEnd) {

		if (dateStart == null || dateEnd == null) {
			return 0;
		}

		LocalDate date1 = new LocalDate(dateStart);
		LocalDate date2 = new LocalDate(dateEnd);
		PeriodType monthDay = PeriodType.months();
		Period difference = new Period(date1, date2, monthDay);
		int months = difference.getMonths();
		return months + 1;
	}

	@WebMethod(operationName = "convertToDate")
	public Date convertToDate(@WebParam(name = "day") final int day, @WebParam(name = "month") final int month,
			@WebParam(name = "year") final int year) {
		return (new DateTime(year, month, day, 0, 0)).toDate();
	}

	@WebMethod(operationName = "validateNoFuture")
	public Date validateNoFuture(@WebParam(name = "date") final Date date) {
		if (date == null)
			return date;

		Calendar calendar = Calendar.getInstance();
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date);

		if (calendar1.before(calendar))
			return null;

		return date;

	}

}
