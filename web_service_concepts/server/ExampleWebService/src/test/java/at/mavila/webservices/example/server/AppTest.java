package at.mavila.webservices.example.server;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;


import static org.assertj.core.api.Assertions.*;

/**
 * Created by marcotulioavilaceron on 16/04/16.
 */
public class AppTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppTest.class);


    @BeforeClass
    public void setUp() {
        BasicConfigurator.configure();
    }

    @Test
    public void testThatDifferenceInMonthsIs3() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 3);
        calendar.set(Calendar.YEAR, 2000);

        calendar.set(Calendar.MONTH, Calendar.MARCH);


        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 25);
        calendar2.set(Calendar.YEAR, 2010);
        calendar2.set(Calendar.MONTH, Calendar.JULY);

        App app = new App();
        int months = app.calculateMonths(calendar.getTime(), calendar2.getTime());

        LOGGER.debug("Months {}", Integer.toString(months));


        assertThat(months).isEqualTo(125);


    }

    @Test
    public void testThatDifferenceInMonthsIs0Month() {

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 29);
        calendar2.set(Calendar.YEAR, 2016);
        calendar2.set(Calendar.MONTH, Calendar.FEBRUARY);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, Calendar.MARCH);

        App app = new App();
        int months = app.calculateMonths(calendar2.getTime(), calendar.getTime());

        LOGGER.debug("Months {}", Integer.toString(months));
        assertThat(months).isEqualTo(1);
    }

    @Test
    public void testThatDifferenceInMonthsIs1Month() {

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 29);
        calendar2.set(Calendar.YEAR, 2016);
        calendar2.set(Calendar.MONTH, Calendar.FEBRUARY);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, Calendar.APRIL);

        App app = new App();
        int months = app.calculateMonths(calendar2.getTime(), calendar.getTime());

        LOGGER.debug("Months {}", Integer.toString(months));
        assertThat(months).isEqualTo(2);
    }

    @Test
    public void testThatDifferenceInMonthsIs12Month() {

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 1);
        calendar2.set(Calendar.YEAR, 2014);
        calendar2.set(Calendar.MONTH, Calendar.MARCH);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 28);
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, Calendar.FEBRUARY);

        App app = new App();
        int months = app.calculateMonths(calendar2.getTime(), calendar.getTime());

        LOGGER.debug("Months {}", Integer.toString(months));
        assertThat(months).isEqualTo(12);
    }

    @Test
    public void testPastDate() {

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 1);
        calendar2.set(Calendar.YEAR, 2014);
        calendar2.set(Calendar.MONTH, Calendar.MARCH);
        App app = new App();
        Date date = app.validateNoFuture(calendar2.getTime());
        assertThat(date).isNull();
    }

    @Test
    public void testFutureDate() {

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, 1);
        calendar2.set(Calendar.YEAR, 2017);
        calendar2.set(Calendar.MONTH, Calendar.MARCH);
        App app = new App();
        Date date = app.validateNoFuture(calendar2.getTime());
        assertThat(date).isNotNull();
    }


    @Test
    public void testGivenDateIsExpected(){

        final App app = new App();
        final Date date = app.convertToDate(5,1,2014);  //1 = January (not the Calendar representation)

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(Calendar.JANUARY);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(5);
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(2014);
    }

}